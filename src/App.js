import "./App.css";
import { Routes, Route } from "react-router-dom";
import user from "./utilities/user";
import useLocalStorage from "./hooks/useLocalStorage.hook";
import Header from "./components/Header/Header";
import RegisterCar from "./components/RegisterCar/RegisterCar";
import MyVehicles from "./components/MyVehicles/MyVehicles";

const INITIAL_MYCARS = {
  user: user,
  cars: [],
};

function App() {
  const localStorage = useLocalStorage();

  const autoLoginDefaultUser = () => {
    let getSavedData = localStorage.get("myCars");
    let newSavedData = INITIAL_MYCARS;
    if (!getSavedData) {
      newSavedData = { ...INITIAL_MYCARS, cars: getSavedData.cars };
    }
    localStorage.set("myCars", newSavedData);
  };
  autoLoginDefaultUser();

  return (
    <div className="App">
      <Header />
      <Routes>
        <Route path="/" element={<RegisterCar />} />
        <Route path="/registrar" element={<RegisterCar />} />
        <Route path="/mis-vehiculos" element={<MyVehicles />} />
      </Routes>
    </div>
  );
}

export default App;
