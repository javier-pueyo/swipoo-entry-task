import { ChevronDownIcon } from "@heroicons/react/24/outline";
import { Disclosure } from "@headlessui/react";
import TableInformationVehicle from "../../TableInformationVehicle/TableInformationVehicle";
import { useEffect, useState } from "react";

function FileVehicle({ infoCar }) {
  const [valueTaxAuthorities, setvalueTaxAuthorities] = useState("");
  const [valueDifferente, setvalueDifferente] = useState("");
  const [valueDepreciated, setvalueDepreciated] = useState("");

  const rawNumberToEuroFormat = (rawNumber) => {
    return new Intl.NumberFormat("es-ES", {
      style: "currency",
      currency: "EUR",
    }).format(rawNumber);
  };

  const caculateValueDifference = (valueVehicle) => {
    const currentTime = new Date();
    const currentYear = currentTime.getFullYear();
    const differenceYears = currentYear - Number(infoCar.enrollmentYear);
    const coefficientDepreciated = 10 * differenceYears;
    const rawValueDifference = (valueVehicle * coefficientDepreciated) / 100;
    return rawValueDifference;
  };

  const calculateValueMarket = (valueTaxAuthorities, differenceValue) => {
    return valueTaxAuthorities - differenceValue;
  };

  useEffect(() => {
    setvalueTaxAuthorities(rawNumberToEuroFormat(infoCar.value));
    const rawValueDifference = caculateValueDifference(infoCar.value);
    const formatValueDifference = rawNumberToEuroFormat(rawValueDifference);
    setvalueDifferente(`- ${formatValueDifference}`);
    const formatValueDepreciated = rawNumberToEuroFormat(
      calculateValueMarket(infoCar.value, rawValueDifference)
    );
    setvalueDepreciated(formatValueDepreciated);
  }, []);

  return (
    <div className="mx-auto max-w-5xl py-8 px-2 sm:px-6 lg:px-8 flex items-center justify-center">
      <div className="overflow-hidden w-full shadow sm:rounded-md">
        <div className="bg-white px-4 py-5 sm:p-6">
          <div className="px-4 pt-5 sm:px-6">
            <Disclosure>
              <h3 className="text-lg font-medium leading-6 text-gray-900">
                {infoCar.brand}
              </h3>
              <Disclosure.Button className="py-2">
                <div
                  type="button"
                  className="flex items-center justify-between w-full py-5"
                >
                  <span className="max-w-2xl text-sm font-medium text-left text-gray-500">
                    Ver más información
                  </span>
                  <ChevronDownIcon
                    className="mx-2 w-4 h-4 shrink-0"
                    aria-hidden="true"
                  />
                </div>
              </Disclosure.Button>
              <Disclosure.Panel className="text-gray-500">
                <TableInformationVehicle carInfo={infoCar} />
              </Disclosure.Panel>
            </Disclosure>
          </div>
        </div>
        <div className="bg-gray-50 px-4 py-5 sm:p-6">
          <div className="px-4 py-5 sm:px-6">
            <dl>
              <div className="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                <dt className="text-sm font-medium text-gray-500">
                  Valoración según Hacienda
                </dt>
                <dd className="text-right mt-1 text-sm text-gray-900 sm:col-span-2 sm:mt-0">
                  {valueTaxAuthorities}
                </dd>
              </div>
              <div className="bg-white px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                <dt className="text-sm font-medium text-gray-500">
                  Depreciación (10% menos cada año)
                </dt>
                <dd className="text-right mt-1 text-sm text-gray-900 sm:col-span-2 sm:mt-0">
                  {valueDifferente}
                </dd>
              </div>
              <div className="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                <dt className="text-sm font-bold text-gray-500">
                  Valoración venal
                </dt>
                <dd className="text-right font-bold mt-1 text-sm text-gray-900 sm:col-span-2 sm:mt-0">
                  {valueDepreciated}
                </dd>
              </div>
            </dl>
          </div>
        </div>
      </div>
    </div>
  );
}

export default FileVehicle;
