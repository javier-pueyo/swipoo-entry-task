import React, { useEffect } from "react";
import FileVehicle from "./FileVehicle/FileVehicle";
import useLocalStorage from "../../hooks/useLocalStorage.hook";
import { useState } from "react";
import { NavLink } from "react-router-dom";

const INITIAL_MYCARS = {
  user: {},
  cars: [],
};

function MyVehicles() {
  const localStorage = useLocalStorage();
  const [dataMyVehicles, setdataMyVehicles] = useState(INITIAL_MYCARS);

  useEffect(() => {
    const myCars = localStorage.get("myCars");
    setdataMyVehicles(myCars);
  }, []);

  return (
    <>
      {dataMyVehicles.cars.length === 0 && (
        <div className="flex flex-col	items-center justify-center h-screen	w-screen	">
          <p className="text-3xl font-bold mb-6">
            No tienes ningun vehículo registrado
          </p>
          <NavLink
            to={"/registrar"}
            type="submit"
            className="inline-flex justify-center rounded-md border border-transparent bg-indigo-600 py-2 px-4 text-sm font-medium text-white shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
          >
            Registrar vehículo
          </NavLink>
        </div>
      )}
      {dataMyVehicles.cars.map((car, index) => (
        <FileVehicle infoCar={car} key={`${JSON.stringify(car)}-${index}`} />
      ))}
    </>
  );
}

export default MyVehicles;
