import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import TableInformationVehicle from "../TableInformationVehicle/TableInformationVehicle";
import useSwipooApi from "../../hooks/swipooApi.service";
import { useFormik } from "formik";
import useLocalStorage from "../../hooks/useLocalStorage.hook";

const INITIAL_LISTCARMODEL = {
  emptyList: true,
  cars: [],
};

function RegisterCar() {
  const localStorage = useLocalStorage();
  const swipooApi = useSwipooApi();
  const navigate = useNavigate();
  const [findedCar, setfindedCar] = useState(false);
  const [listCarModel, setlistCarModel] = useState(INITIAL_LISTCARMODEL);
  const [selectedModel, setSelectedModel] = useState({});

  const validate = (values) => {
    const errors = {};

    if (values.brand === "DEFAULT") {
      errors.brand = "Debes seleccionar una opción";
    }

    if (values.fuel === "DEFAULT") {
      errors.fuel = "Debes seleccionar una opción";
    }

    if (!values.enrollmentDay) {
      errors.enrollmentDay = "Requerido";
    } else if (!/^[0-9.]+$/i.test(values.enrollmentDay)) {
      errors.enrollmentDay = "Invalido formato de fecha";
    } else if (
      Number(values.enrollmentDay) < 1 ||
      Number(values.enrollmentDay) > 31
    ) {
      errors.enrollmentDay = "Indica un número de día válido";
    }

    if (!values.enrollmentMonth) {
      errors.enrollmentMonth = "Requerido";
    } else if (!/^[0-9.]+$/i.test(values.enrollmentMonth)) {
      errors.enrollmentMonth = "Invalido formato de fecha";
    } else if (
      Number(values.enrollmentMonth) < 1 ||
      Number(values.enrollmentMonth) > 12
    ) {
      errors.enrollmentMonth = "Indica un número de mes válido";
    }

    if (!values.enrollmentYear) {
      errors.enrollmentYear = "Requerido";
    } else if (!/^[0-9.]+$/i.test(values.enrollmentYear)) {
      errors.enrollmentYear = "Invalido formato de fecha";
    } else if (
      Number(values.enrollmentYear) < 1500 ||
      Number(values.enrollmentYear) > 3000
    ) {
      errors.enrollmentYear = "Indica un número de año válido";
    }

    if (values.model === "DEFAULT") {
      errors.model = "Debes seleccionar una opción";
    }

    return errors;
  };

  const formik = useFormik({
    initialValues: {
      brand: "DEFAULT",
      enrollmentDay: "",
      enrollmentMonth: "",
      enrollmentYear: "",
      fuel: "DEFAULT",
      model: "DEFAULT",
    },
    validate,
    onSubmit: (values) => {
      pushNewDataToLocalStorage(values);
      navigate("/mis-vehiculos");
    },
  });

  // Push to LocalStore Object with the user data and her registed car.
  const pushNewDataToLocalStorage = (values) => {
    let myCars = localStorage.get("myCars");
    const newdataCar = { ...values, ...selectedModel[0] };
    myCars.cars.push(newdataCar);
    localStorage.set("myCars", myCars);
  };

  const readyForFindCar = () => {
    if (
      formik.values.enrollmentYear &&
      Number(formik.values.enrollmentYear) > 1500 &&
      formik.values.enrollmentMonth &&
      formik.values.enrollmentDay &&
      formik.values.fuel !== "DEFAULT" &&
      formik.values.brand !== "DEFAULT"
    ) {
      return true;
    }
    return false;
  };

  const getListCarModel = async () => {
    const params = {
      brand: formik.values.brand,
      fuel: formik.values.fuel,
      day: formik.values.enrollmentDay,
      month: formik.values.enrollmentMonth,
      year: formik.values.enrollmentYear,
    };
    setlistCarModel(await swipooApi.fetchCheckCarModel(params));
    formik.values.model = "DEFAULT";
  };

  const getFirstListCarModel = async () => {
    await getListCarModel();
    setfindedCar(true);
  };

  const showOrHideListCarModel = () => {
    if (readyForFindCar() && findedCar !== true) {
      getFirstListCarModel();
    } else if (readyForFindCar()) {
      getListCarModel();
    } else if (!readyForFindCar()) {
      setfindedCar(false);
    }
  };

  useEffect(() => {
    showOrHideListCarModel();
  }, [
    formik.values.brand,
    formik.values.fuel,
    formik.values.enrollmentDay,
    formik.values.enrollmentMonth,
    formik.values.enrollmentYear,
    findedCar,
  ]);

  const UpdateTableInformation = () => {
    const selectedModel = listCarModel.cars.filter(
      (car) => car.model === formik.values.model
    );
    setSelectedModel(selectedModel);
  };

  useEffect(() => {
    UpdateTableInformation();
  }, [formik.values.model]);

  return (
    <div className="mx-auto max-w-7xl py-8 px-2 sm:px-6 lg:px-8 flex items-center justify-center">
      <div className="relative">
        <form onSubmit={formik.handleSubmit}>
          <div className="overflow-hidden shadow sm:rounded-md">
            <div className="bg-white px-4 py-5 sm:p-6">
              <div className="grid grid-cols-6 gap-6">
                <div className="col-span-6">
                  <label
                    htmlFor="brand"
                    className="block text-sm font-medium text-gray-700"
                  >
                    Marca
                  </label>
                  <select
                    name="brand"
                    value={formik.values.brand}
                    onChange={formik.handleChange}
                    className="mt-1 block w-full rounded-md border border-gray-300 bg-white py-2 px-3 shadow-sm focus:border-blue-500 focus:outline-none focus:ring-blue-500 sm:text-sm"
                  >
                    <option
                      className="hidden"
                      value="DEFAULT"
                      disabled="disabled"
                    >
                      Seleccionar una opción
                    </option>
                    <option value="Abarth">Abarth</option>
                    <option value="Alfa Romeo">Alfa Romeo</option>
                    <option value="Alpine">Alpine</option>
                    <option value="ARO">ARO</option>
                    <option value="Aston Martin">Aston Martin</option>
                    <option value="Audi">Audi</option>
                    <option value="Austin">Austin</option>
                    <option value="Bentley">Bentley</option>
                    <option value="BMW">BMW</option>
                    <option value="BYD">BYD</option>
                    <option value="Cadillac">Cadillac</option>
                    <option value="Caterham">Caterham</option>
                    <option value="Chevrolet">Chevrolet</option>
                    <option value="Chrysler">Chrysler</option>
                    <option value="Citroen">Citroen</option>
                    <option value="Corvette">Corvette</option>
                    <option value="Cupra">Cupra</option>
                    <option value="Dacia">Dacia</option>
                    <option value="Daewoo">Daewoo</option>
                    <option value="Daihatsu">Daihatsu</option>
                    <option value="Daimler">Daimler</option>
                    <option value="De Tomaso">De Tomaso</option>
                    <option value="Dodge">Dodge</option>
                    <option value="Dr Motor">Dr Motor</option>
                    <option value="DS">DS</option>
                    <option value="Ferrari">Ferrari</option>
                    <option value="Fiat">Fiat</option>
                    <option value="Fisker">Fisker</option>
                    <option value="Ford">Ford</option>
                    <option value="FSM">FSM</option>
                    <option value="Galloper">Galloper</option>
                    <option value="Honda">Honda</option>
                    <option value="Hummer">Hummer</option>
                    <option value="Hurtan">Hurtan</option>
                    <option value="Hyundai">Hyundai</option>
                    <option value="Infiniti">Infiniti</option>
                    <option value="Infinity">Infinity</option>
                    <option value="Innocenti">Innocenti</option>
                    <option value="Isuzu">Isuzu</option>
                    <option value="Iveco">Iveco</option>
                    <option value="Jaguar">Jaguar</option>
                    <option value="Jeep">Jeep</option>
                    <option value="Kia">Kia</option>
                    <option value="Lada">Lada</option>
                    <option value="Lamborghini">Lamborghini</option>
                    <option value="Lancia">Lancia</option>
                    <option value="Land Rover">Land Rover</option>
                    <option value="Lexus">Lexus</option>
                    <option value="Lotus">Lotus</option>
                    <option value="Mahindra">Mahindra</option>
                    <option value="Maserati">Maserati</option>
                    <option value="Maybach">Maybach</option>
                    <option value="Mazda">Mazda</option>
                    <option value="Mclaren">Mclaren</option>
                    <option value="Mercedes">Mercedes</option>
                    <option value="Mini">Mini</option>
                    <option value="Mitsubishi">Mitsubishi</option>
                    <option value="Mk Sportscar">Mk Sportscar</option>
                    <option value="Morgan">Morgan</option>
                    <option value="Nissan">Nissan</option>
                    <option value="Opel">Opel</option>
                    <option value="Peugeot">Peugeot</option>
                    <option value="PGO">PGO</option>
                    <option value="Piaggio">Piaggio</option>
                    <option value="Pontiac">Pontiac</option>
                    <option value="Porsche">Porsche</option>
                    <option value="Qoros">Qoros</option>
                    <option value="Renault">Renault</option>
                    <option value="Rolls Royce">Rolls Royce</option>
                    <option value="Rover">Rover</option>
                    <option value="SAAB">SAAB</option>
                    <option value="Santana">Santana</option>
                    <option value="SEAT">SEAT</option>
                    <option value="Skoda">Skoda</option>
                    <option value="Smart">Smart</option>
                    <option value="Ssangyong">Ssangyong</option>
                    <option value="Subaru">Subaru</option>
                    <option value="Suzuki">Suzuki</option>
                    <option value="Tata">Tata</option>
                    <option value="Tesla">Tesla</option>
                    <option value="Think">Think</option>
                    <option value="Toyota">Toyota</option>
                    <option value="Volkswagen">Volkswagen</option>
                    <option value="Volvo">Volvo</option>
                  </select>
                  {formik.touched.brand && formik.errors.brand ? (
                    <p className="mt-2 text-sm text-red-600 dark:text-red-500">
                      {formik.errors.brand}
                    </p>
                  ) : null}
                </div>

                <div className="col-span-6">
                  <label
                    htmlFor="country"
                    className="block text-sm font-medium text-gray-700"
                  >
                    Combustible
                  </label>
                  <select
                    name="fuel"
                    value={formik.values.fuel}
                    onChange={formik.handleChange}
                    className="mt-1 block w-full rounded-md border border-gray-300 bg-white py-2 px-3 shadow-sm focus:border-blue-500 focus:outline-none focus:ring-blue-500 sm:text-sm"
                  >
                    <option
                      className="hidden"
                      value="DEFAULT"
                      disabled="disabled"
                    >
                      Seleccionar una opción
                    </option>
                    <option value="D">Diesel</option>
                    <option value="Elc">Eléctrico</option>
                    <option value="G">Gasolina</option>
                    <option value="S">Gas</option>
                    <option value="DyE">Diesel y Eléctrico</option>
                    <option value="GyE">Gasolina y Eléctrico</option>
                  </select>
                  {formik.touched.fuel && formik.errors.fuel ? (
                    <p className="mt-2 text-sm text-red-600 dark:text-red-500">
                      {formik.errors.fuel}
                    </p>
                  ) : null}
                </div>

                <div className="col-span-6">
                  <label
                    htmlFor="company-website"
                    className="block text-sm font-medium text-gray-700"
                  >
                    Fecha de matriculación
                  </label>
                </div>

                <div className="col-span-6 sm:col-span-6 lg:col-span-2">
                  <div className="mt-1 flex rounded-md shadow-sm">
                    <span className="inline-flex items-center rounded-l-md border border-r-0 border-gray-300 bg-gray-50 px-3 text-sm text-gray-500">
                      Dia
                    </span>
                    <input
                      type="text"
                      name="enrollmentDay"
                      pattern="[0-9.]+"
                      value={formik.values.enrollmentDay}
                      onChange={formik.handleChange}
                      className="block w-full flex-1 rounded-none rounded-r-md border-gray-300 focus:border-blue-500 focus:ring-blue-500 sm:text-sm"
                      placeholder="06"
                    />
                  </div>
                  {formik.touched.enrollmentDay &&
                  formik.errors.enrollmentDay ? (
                    <p className="mt-2 text-sm text-red-600 dark:text-red-500">
                      {formik.errors.enrollmentDay}
                    </p>
                  ) : null}
                </div>

                <div className="col-span-6 sm:col-span-6 lg:col-span-2">
                  <div className="mt-1 flex rounded-md shadow-sm">
                    <span className="inline-flex items-center rounded-l-md border border-r-0 border-gray-300 bg-gray-50 px-3 text-sm text-gray-500">
                      Mes
                    </span>
                    <input
                      type="text"
                      name="enrollmentMonth"
                      pattern="[0-9.]+"
                      value={formik.values.enrollmentMonth}
                      onChange={formik.handleChange}
                      className="block w-full flex-1 rounded-none rounded-r-md border-gray-300 focus:border-blue-500 focus:ring-blue-500 sm:text-sm"
                      placeholder="02"
                    />
                  </div>
                  {formik.touched.enrollmentMonth &&
                  formik.errors.enrollmentMonth ? (
                    <p className="mt-2 text-sm text-red-600 dark:text-red-500">
                      {formik.errors.enrollmentMonth}
                    </p>
                  ) : null}
                </div>

                <div className="col-span-6 sm:col-span-6 lg:col-span-2">
                  <div className="mt-1 flex rounded-md shadow-sm">
                    <span className="inline-flex items-center rounded-l-md border border-r-0 border-gray-300 bg-gray-50 px-3 text-sm text-gray-500">
                      Año
                    </span>
                    <input
                      type="text"
                      name="enrollmentYear"
                      pattern="[0-9.]+"
                      value={formik.values.enrollmentYear}
                      onChange={formik.handleChange}
                      className="block w-full flex-1 rounded-none rounded-r-md border-gray-300 focus:border-blue-500 focus:ring-blue-500 sm:text-sm"
                      placeholder="2015"
                    />
                  </div>
                  {formik.touched.enrollmentYear &&
                  formik.errors.enrollmentYear ? (
                    <p className="mt-2 text-sm text-red-600 dark:text-red-500">
                      {formik.errors.enrollmentYear}
                    </p>
                  ) : null}
                </div>

                {findedCar && !listCarModel.emptyList && (
                  <div className="col-span-6">
                    <label
                      htmlFor="country"
                      className="block text-sm font-medium text-gray-700"
                    >
                      Modelo
                    </label>
                    <select
                      name="model"
                      value={formik.values.model}
                      onChange={formik.handleChange}
                      className="mt-1 block w-full rounded-md border border-gray-300 bg-white py-2 px-3 shadow-sm focus:border-blue-500 focus:outline-none focus:ring-blue-500 sm:text-sm"
                    >
                      <option
                        className="hidden"
                        value="DEFAULT"
                        disabled="disabled"
                      >
                        Selecciona un modelo
                      </option>
                      {listCarModel.cars.map((carModel, index) => (
                        <option
                          key={`${JSON.stringify(carModel.model)}-${index}`}
                        >
                          {carModel.model}
                        </option>
                      ))}
                    </select>
                    {formik.touched.model && formik.errors.model ? (
                      <p className="mt-2 text-sm text-red-600 dark:text-red-500">
                        {formik.errors.model}
                      </p>
                    ) : null}
                  </div>
                )}
                {findedCar && listCarModel.emptyList && (
                  <div className="col-span-6">
                    <p className="mt-2 text-sm text-red-600 dark:text-red-500">
                      No se ha encontrado ningún vehículo con estas
                      características
                    </p>
                  </div>
                )}
              </div>
            </div>
            <div className="bg-gray-50 px-4 py-3 text-right sm:px-6">
              {selectedModel.length !== 0 && !listCarModel.emptyList && (
                <TableInformationVehicle carInfo={selectedModel[0]} />
              )}
              <button
                type="submit"
                className="inline-flex justify-center rounded-md border border-transparent bg-blue-600 py-2 px-4 text-sm font-medium text-white shadow-sm hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-blue-500 focus:ring-offset-2"
              >
                Registrar
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
}

export default RegisterCar;
