import React from "react";
import { useEffect, useState } from "react";

const carFeaturesSpanish = {
  brand: "Marca",
  model: "Modelo",
  cc: "Cilindrada",
  cylinders: "Cilindros",
  fuel: "Combustible",
  kw: "Potencia (KW)",
  cvf: "Potencia fiscal",
  cv: "Potencia (CV)",
};

function TableInformationVehicle({ carInfo }) {
  const carInformation = carInfo;
  const [mapInformation, setMapInformation] = useState([]);
  let toggleBackgroundColor = false;

  // join the information from check-car-models with the information that should appear in the table
  const mergeInformation = () => {
    let mapInformation = [];
    for (const featureCar in carFeaturesSpanish) {
      for (const infoCar in carInformation) {
        if (infoCar === featureCar) {
          mapInformation.push([
            carFeaturesSpanish[featureCar],
            carInformation[infoCar],
          ]);
          break;
        }
      }
    }
    return mapInformation;
  };

  useEffect(() => {
    setMapInformation(mergeInformation());
  }, [carInfo]);

  return (
    <div className="py-2 sm:px-6">
      <dl>
        {mapInformation.map((info) => {
          toggleBackgroundColor = !toggleBackgroundColor;
          return (
            <div
              key={`${JSON.stringify(carInfo.model)}-${info[0]}`}
              className={`
                ${toggleBackgroundColor ? "bg-gray-50" : "bg-white"}
                px-4 py-3 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6
                `}
            >
              <dt className="text-left text-sm font-medium text-gray-500">
                {info[0]}
              </dt>
              <dd className="text-right mt-1 text-sm text-gray-900 sm:col-span-2 sm:mt-0">
                {info[1]}
              </dd>
            </div>
          );
        })}
      </dl>
    </div>
  );
}

export default TableInformationVehicle;
