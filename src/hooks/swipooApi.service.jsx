import Axios from "axios";

const useSwipooApi = () => {
  const fetchCheckCarModel = async (params) => {
    const { brand, fuel, day, month, year } = params;
    const carList = {
      emptyList: true,
      cars: [],
    };
    const urlCheckCarModel =
      "https://api-sandbox.swipoo.com/v1/check-car-models";
    const paramBrand = brand;
    const paramFuel = fuel;
    const paramEnrollmentDate = year + "/" + month + "/" + day;
    try {
      const resp = await Axios.get(urlCheckCarModel, {
        params: {
          brand: paramBrand,
          enrollmentDate: paramEnrollmentDate,
          fuel: paramFuel,
        },
      });
      carList.cars = resp.data.cars;
      if (carList.cars.length !== 0) {
        carList.emptyList = false;
      }
      return carList;
    } catch (err) {
      // Handle Error Here
      console.error(err);
    }
  };

  return {
    fetchCheckCarModel,
  };
};

export default useSwipooApi;
