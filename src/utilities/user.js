const user = {
  id: "001",
  name: "Javier Pueyo",
  surname: "Pueyo",
  dni: "43560708L",
  province: "Barcelona",
  ccaa: "",
  zipCode: "",
  phone: "",
  email: "pueyo.mir@gmail.com",
  city: "Barcelona",
  address: "",
  photoDni: "",
  birthDate: "0000-00-00",
  census: "",
};
export default user;
